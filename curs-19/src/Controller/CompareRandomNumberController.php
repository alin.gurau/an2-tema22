<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class CompareRandomNumberController extends AbstractController{
    
    /**
     * @Route("/compareNumber", name="compare-number")
     */
    public function compareRandomNumber(){
        $randomNumber = rand(0,100);
        $_GET["insertedNumber"];

        return $this->render('random-number.html.twig',['randomNumber'=>$randomNumber]);
    }
}
?>